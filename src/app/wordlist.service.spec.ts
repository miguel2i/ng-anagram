import { TestBed, inject } from '@angular/core/testing';

import { WordlistService } from './wordlist.service';

describe('WordlistService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WordlistService]
    });
  });

  it('should be created', inject([WordlistService], (service: WordlistService) => {
    expect(service).toBeTruthy();
  }));
});
