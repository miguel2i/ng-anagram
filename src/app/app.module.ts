import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http'; 

import { AppComponent } from './app.component';
import { LetterComponent } from './letter/letter.component';
import { ChoiceLetterComponent } from './choice-letter/choice-letter.component';
import { WordsComponent } from './words/words.component';
import { WordComponent } from './word/word.component';
import { WordlistService } from './wordlist.service';

@NgModule({
  declarations: [
    AppComponent,
    LetterComponent,
    ChoiceLetterComponent,
    WordsComponent,
    WordComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [
    WordlistService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
