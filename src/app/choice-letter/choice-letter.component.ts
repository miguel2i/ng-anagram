import { Component, Input, Output, OnChanges, EventEmitter } from '@angular/core';
import {LetterComponent } from '../letter/letter.component';

@Component({
  selector: 'app-choice-letter',
  templateUrl: './choice-letter.component.html',
  styleUrls: ['./choice-letter.component.css']
})
export class ChoiceLetterComponent implements OnChanges {
  @Input() letters: string;
  @Output() onValid = new EventEmitter<String>();
  choiceletters: Array<string>;
  guessletters: Array<string>;

  constructor() { }

  ngOnChanges(changes) {
    if(changes.letters.currentValue) {
      this.initLetters(changes.letters.currentValue);
    }
  }

  initLetters(letters:string ){
    this.choiceletters = letters.split("");
    this.guessletters = this.choiceletters.map( e=> '');
  }

  onClear() {
    let gl = this.guessletters.filter(( l)=> l!='');
    let letters = this.choiceletters.map((l) => l == '' ? gl.splice(0,1)[0] : l );
    
    this.initLetters(letters.join(""));
  }

  moveLetterDown(event: LetterComponent){
    let dest_pos: number = this.guessletters.indexOf('');
    let src_pos = event.position;
    this.guessletters[dest_pos] = this.choiceletters[src_pos];
    this.choiceletters[src_pos] = '';
  }

  moveLetterUp(event: LetterComponent) {
    let dest_pos = this.choiceletters.indexOf('');
    let src_pos = event.position;
    this.choiceletters[dest_pos] = this.guessletters[src_pos];
    this.guessletters[src_pos]='';
  }

  getWord() {
    return this.guessletters.join('');
  }

  valid() {
    let word = this.getWord();
    if (word){
      this.onValid.emit(word);
      this.onClear();
    }
  }
}

