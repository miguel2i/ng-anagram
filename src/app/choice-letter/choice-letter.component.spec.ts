import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoiceLetterComponent } from './choice-letter.component';

describe('ChoiceLetterComponent', () => {
  let component: ChoiceLetterComponent;
  let fixture: ComponentFixture<ChoiceLetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoiceLetterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoiceLetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
