import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-word',
  templateUrl: './word.component.html',
  styleUrls: ['./word.component.css']
})
export class WordComponent implements OnInit {
  @Input() name: string;
  @Input() foundByMe: boolean = false;
  @Input() foundByOther: boolean = false;
  mask: string;

  constructor() { }

  ngOnInit() {
    const m : string = '-';
    this.mask = m.repeat(this.name.length);
  }
}
