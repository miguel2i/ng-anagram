import { Injectable } from '@angular/core';
import { Http} from '@angular/http';

@Injectable()
export class WordlistService {
  constructor(private http: Http) {
  }

  getList() {
    return this.http.get("http://localhost:8000/word");
    // return { 'letters' : 'abcdef' , 'list' : ['abc', 'def', 'bcd', 'abcdef']};
  }

}
