import { Component } from '@angular/core';
import { WordlistService } from './wordlist.service';
import { ChoiceLetterComponent } from './choice-letter/choice-letter.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  letters: string;
  wordlist: Array<any>;

  constructor(private wls: WordlistService) {
    this.wordlist = [];
    this.letters = 'xxxxxx';
    this.loadList();
  }

  loadList(){
    this.wls.getList().subscribe(reponse => {
      let json = reponse.json();
      this.wordlist= json.list.map(name => ({name: name.toUpperCase(), foundByMe: false, foundByOther: false}));
      this.letters = json.letter.toUpperCase();
    });
  }

  foundWord(word: string, byMe: boolean) {
    this.wordlist = this.wordlist.map(
       w => {
         if (w.name == word){
           if(byMe) {
             w.foundByMe = true;
           } else {
             w.foundByOther = true;
           }
         }
         return w;
      });
  }

  onValidWord(w: string) {
    console.log(w);
    console.log(this.wordlist);
    if (this.wordlist.some(a => (a.name === w))) {
      console.log("word in list");
      this.foundWord(w, true);
    } else {
      console.log("word invalid");
    }
  }
}

