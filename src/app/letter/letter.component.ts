import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-letter',
  templateUrl: './letter.component.html',
  styleUrls: ['./letter.component.css']
})
export class LetterComponent {
  @Input() letter: string ;
  @Input() position: number;
  @Output() move = new EventEmitter<LetterComponent>();
  constructor() { }

  btnclick() {
    this.move.emit(this);
  }
}


